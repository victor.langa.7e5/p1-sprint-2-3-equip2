using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public void Start()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        GameObject.Find("Transition").GetComponent<Transitions>().loadedScene = true;
    }

    public void LoadGame()
    {     
        StartCoroutine(LoadAwaiter(2, 1));
    }
    public void LoadResults()
    {
        StartCoroutine(LoadAwaiter(2, 2));
    }
    public void LoadMain()
    {
        StartCoroutine(LoadAwaiter(2, 0));
    }

    public void QuitGame()
    {
        Application.Quit();
    }
    IEnumerator LoadAwaiter(float waitTime, int SceneId)
    {
        Destroy(GameObject.Find("EventSystem"));
        GameObject.Find("Transition").GetComponent<Transitions>().SceneExit();
        yield return new WaitForSeconds(waitTime);
        SceneManager.LoadSceneAsync(SceneId);

    }
}
