using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Transitions : MonoBehaviour
{
    public bool SceneOut = false;
    public bool loadedScene = false;

    float posX;
    // Start is called before the first frame update
    void Start()
    {
        posX = 0;
        transform.localPosition = new Vector3(posX, 0);
       
    }

    // Update is called once per frame
    void Update()
    {

        posX = transform.localPosition.x;
        if (loadedScene && posX >= -4000f)
        {
            transform.localPosition -= new Vector3(2000 * Time.deltaTime, 0);
            Debug.Log("Position1 updated");
        }
        if (SceneOut && posX >= 0f)
        {
            transform.localPosition -= new Vector3(2000 * Time.deltaTime, 0);
            Debug.Log("Position2 updated");
        }
    }
    public void SceneExit()
    {
        posX = 3000;
        SceneOut = true;
        loadedScene = false;
        transform.localPosition = new Vector3(posX, 0);
        Debug.Log("Scene exit?");  
    }
}

