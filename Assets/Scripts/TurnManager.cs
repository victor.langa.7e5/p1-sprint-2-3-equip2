using UnityEngine;
using UnityEngine.SceneManagement;

public class TurnManager : MonoBehaviour
{
    const float InitialPointTurns = 100f;
    const int Turns = 10;
    const float InitialAlpha = 1f;
    const float SecondsBetweenColorChange = 0.4f;

    private MatrixManager _matrixManager;
    private SpriteRenderer _background;
    private SpriteRenderer _anim;

    public float _totalColorblindPoints = 0f;
    public float _redColorblindPoints = 0f;
    public float _greenColorblindPoints = 0f;
    public float _blueColorBlindPoints = 0f;
    public float _totalScore = 0f;
   
    public float _timer = 1f;
    public bool _animTriggered;
    public float _currentTimer;

    private int _result;

    private int _level = 1;
    private string _mode = "b/w";
    private float _pointsTurn = InitialPointTurns;
    private int _activeTurn = 1;
    private bool _doubleCheck = false;
    private float _alpha = InitialAlpha;

    private float musicVolume;

    Color[] _matrixGreyColors = new Color[]
    {
        Color.HSVToRGB(0f, 0f, 0f),
        Color.HSVToRGB(0f, 0f, 0.1f),
        Color.HSVToRGB(0f, 0f, 0.2f)
    };
    Color[] _answerMatrixGreyColors = new Color[]
    {
        Color.HSVToRGB(0f, 0f, 0.8f),
        Color.HSVToRGB(0f, 0f, 0.9f),
        Color.HSVToRGB(0f, 0f, 1f)
    };
    Color[] _matrixRedColors = new Color[]
    {
        Color.HSVToRGB(0f, 1f, 1f),
        Color.HSVToRGB(0f, 1f, 0.8f),
        Color.HSVToRGB(0f, 1f, 0.6f)
    };
    Color[] _answerMatrixRedColors = new Color[]
    {
        Color.HSVToRGB(0.3f, 1f, 1f),
        Color.HSVToRGB(0.3f, 1f, 0.75f),
        Color.HSVToRGB(0.3f, 1f, 0.5f)
    };
    Color[] _matrixGreenColors = new Color[]
    {
        Color.HSVToRGB(0.3f, 1f, 1f),
        Color.HSVToRGB(0.3f, 1f, 0.8f),
        Color.HSVToRGB(0.3f, 1f, 0.6f)
    };
    Color[] _answerMatrixGreenColors = new Color[]
    {
        Color.HSVToRGB(0f, 1f, 1f),
        Color.HSVToRGB(0f, 1f, 0.8f),
        Color.HSVToRGB(0f, 1f, 0.6f)
    };

    Color[] _matrixBlueColors = new Color[]
    {
        Color.HSVToRGB(0.15f, 0.9f, 1f),
        Color.HSVToRGB(0.15f, 0.9f, 0.9f),
        Color.HSVToRGB(0.15f, 0.9f, 0.8f)
    };
    Color[] _answerMatrixBlueColors = new Color[]
    {
        Color.HSVToRGB(0.72f, 0.9f, 1f),
        Color.HSVToRGB(0.72f, 0.9f, 0.9f),
        Color.HSVToRGB(0.72f, 0.9f, 0.8f)
    };

    private float _counter = 0f;

    private void Start()
    {
        _matrixManager = GetComponent<MatrixManager>();
        _background = GameObject.Find("Background").GetComponent<SpriteRenderer>();
        _anim = GameObject.Find("Animation").GetComponent<SpriteRenderer>();
        ColorMatrixSquares();
        UIUpdate();
    }

    private void Update()
    {
        musicVolume = PlayerPrefs.GetFloat("Music volume");
        _counter += Time.deltaTime;
        if (_counter >= SecondsBetweenColorChange)
        {
            _counter = 0;
            ColorMatrixSquares();
        }
        Color colorAnim = _anim.color;
        if (_animTriggered)
        {
            colorAnim.a = _timer -_currentTimer;
            _anim.color = colorAnim;
            _currentTimer += Time.deltaTime*2;
            if (_currentTimer >= _timer)
                _animTriggered = false;
        }
        GameObject.Find("Song1").GetComponent<AudioSource>().volume = musicVolume;
        GameObject.Find("Song2").GetComponent<AudioSource>().volume = musicVolume;
        GameObject.Find("Song3").GetComponent<AudioSource>().volume = musicVolume;
    }

    public void CheckTurn(int result)
    {
        _result = result;

        switch (ProcessAnswer())
        {
            case 1:
                TriggerError();
                break;
            case 2:
                TriggerSuccess();
                NextTurn();
                break;
        }

        if (_mode == "endgame")
        {
            PlayerPrefs.SetFloat("totalColorBlind", _totalColorblindPoints);
            PlayerPrefs.SetFloat("redColorBlind", _redColorblindPoints);
            PlayerPrefs.SetFloat("greenColorBlind", _greenColorblindPoints);
            PlayerPrefs.SetFloat("blueColorBlind", _blueColorBlindPoints);
            PlayerPrefs.SetFloat("score", _totalScore);

            Destroy(GameObject.Find("MouseInput"));
            GameObject.Find("Canvas").GetComponent<SceneLoader>().LoadResults();
        }
    }

    int ProcessAnswer()
    {
        // result == 1 when only hits 1 object (matrix)
        // result == 2 when hits 2 objects (matrix && answerMatrix)

        switch (_mode, _result)
        {
            case ("b/w", 1):
            case ("red", 1):
            case ("green", 1):
            case ("blue", 1):
                _pointsTurn /= 2f;
                return 1;
            case ("b/w", 2):
                _totalColorblindPoints += _pointsTurn;
                return 2;
            case ("red", 2):
                _redColorblindPoints += _pointsTurn;
                return 2;
            case ("green", 2):
                _greenColorblindPoints += _pointsTurn;
                return 2;
            case ("blue", 2):
                _blueColorBlindPoints += _pointsTurn;
                return 2;
        }

        return 0;
    }

    void TriggerError()
    {
        Animation(0);
        switch (_doubleCheck)
        {
            case true:
                _doubleCheck = false;
                NextRound();
                break;
            case false:
                _doubleCheck = true;
                break;
        }

        // To upgrade in HU10T2
    }

    void TriggerSuccess()
    {
        Animation(1);
        _doubleCheck = false;

        // To upgrade in HU11T1
    }

    void NextTurn()
    {
        if (_mode == "endgame") return;
        if (_activeTurn >= Turns)
            NextRound();
        else
        {
            _activeTurn++;
            _pointsTurn = InitialPointTurns;
            _alpha -= InitialAlpha / Turns;
            _matrixManager.UpdateAnswerObject();
            ColorMatrixSquares();
        }
        UIUpdate();
    }

    public void NextRound()
    {
        _doubleCheck = false;
        _activeTurn = 0;
        _alpha = InitialAlpha + (InitialAlpha / Turns);

        switch (_mode)
        {
            case "b/w":
                GameObject.Find("Song1").GetComponent<AudioSource>().Stop();
                GameObject.Find("Song2").GetComponent<AudioSource>().Play();
                _mode = "red";
                break;
            case "red":
                GameObject.Find("Song2").GetComponent<AudioSource>().Stop();
                GameObject.Find("Song3").GetComponent<AudioSource>().Play();
                _mode = "green";
                break;
            case "green":
                GameObject.Find("Song3").GetComponent<AudioSource>().Stop();
                GameObject.Find("Song1").GetComponent<AudioSource>().Play();
                _mode = "blue";
                break;
            case "blue":
                _mode = "endgame";
                break;

        }
        _level++;
        NextTurn();
    }

    void ColorMatrixSquares()
    {
        Color[] matrixColors = new Color[0];
        Color[] answerMatrixColors = new Color[0];

        switch (_mode)
        {
            case "b/w":
                matrixColors = _matrixGreyColors;
                answerMatrixColors = _answerMatrixGreyColors;
                _background.color = Color.HSVToRGB(0f, 0f, 0.3f);
                break;
            case "red":
                matrixColors = _matrixRedColors;
                answerMatrixColors = _answerMatrixRedColors;
                _background.color = Color.HSVToRGB(0f, 1f, 0.3f);
                break;
            case "green":
                matrixColors = _matrixGreenColors;
                answerMatrixColors = _answerMatrixGreenColors;
                _background.color = Color.HSVToRGB(0.3f, 1f, 0.3f);
                break;
            case "blue":
                matrixColors = _matrixBlueColors;
                answerMatrixColors = _answerMatrixBlueColors;
                _background.color = Color.HSVToRGB(0.15f, 1f, 0.3f);
                break;
        }

        _matrixManager.UpdateMatrixSquareColors(matrixColors);
        _matrixManager.UpdateAnswerSquareColors(answerMatrixColors, _alpha);
    }

    void UIUpdate()
    {
        TMPro.TMP_Text LevelText = GameObject.Find("Level").GetComponent<TMPro.TMP_Text>();
        TMPro.TMP_Text ColorText = GameObject.Find("Colors").GetComponent<TMPro.TMP_Text>();
        TMPro.TMP_Text LevelPText = GameObject.Find("LevelProgress").GetComponent<TMPro.TMP_Text>();
        TMPro.TMP_Text ScoreText = GameObject.Find("Score").GetComponent<TMPro.TMP_Text>();
        _totalScore = _totalColorblindPoints + _redColorblindPoints + _greenColorblindPoints + _blueColorBlindPoints;
        ScoreText.text = "Score: " + _totalScore + " / 4000";
        LevelText.text = "Level: " + _level;
        LevelPText.text = "Level Progress: " + (((float)_activeTurn-1 )/ Turns) * 100 + "%";
        ColorText.text = "Colors: " + _mode;
    }
    void Animation(int mode)
    {
        switch (mode)
        {
            case 0:
                _anim.color = Color.HSVToRGB(0f, 1f, 1f);
                break;
            case 1:
                _anim.color = Color.HSVToRGB(0.3f, 1f, 1f);
                break;
            default:
                Debug.Log("Mode value received doesn't correspond to any mode");
                break;

        }
        _currentTimer = 0;
        _animTriggered = true;
    }
}
