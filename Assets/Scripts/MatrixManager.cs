using Unity.VisualScripting;
using UnityEngine;

public class MatrixManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _matrixSquarePrefab;
    private GameObject _answerMatrix;

    private GameObject[,] _matrixSquaresData;
    private GameObject[,] _answerMatrixSquaresData;

    private int _matrixProportion;
    private int _answerMatrixProportion;

    void Awake()
    {
        _matrixProportion = 16; // Min = _answerMatrixProportion + 1, recommended max = 75
        _answerMatrixProportion = 3; // Min = 1, max = _matrixProportion - 1, needs to be odd

        _matrixSquaresData = GenerateMatrixSquares("Square", false);
        InitializeAnswerObject();
        UpdateAnswerObject();
    }

    GameObject[,] GenerateMatrixSquares(string id, bool isAnswerObject)
    {
        int isAnswerObjectInt;
        switch (isAnswerObject)
        {
            case true:
                isAnswerObjectInt = _matrixProportion - _answerMatrixProportion;
                break;
            case false:
                isAnswerObjectInt = 0;
                break;
        }

        float squaresScale = 1f / _matrixProportion;
        GameObject[,] matrix = 
            new GameObject[_matrixProportion - isAnswerObjectInt, _matrixProportion - isAnswerObjectInt];

        for (int i = 0; i < _matrixProportion - isAnswerObjectInt; i++)
        {
            for (int j = 0; j < _matrixProportion - isAnswerObjectInt; j++)
            {
                GameObject _matrixSquare = Instantiate(_matrixSquarePrefab, transform);
                _matrixSquare.name = $"{id}";

                _matrixSquare.transform.localScale =
                    new Vector3(squaresScale - squaresScale * 0.1f, squaresScale - squaresScale * 0.1f);

                _matrixSquare.transform.localPosition =
                    new Vector3(0f - (0.5f - squaresScale / 2f) + (i * squaresScale),
                    0f - (0.5f - squaresScale / 2f) + (j * squaresScale));

                matrix[i, j] = _matrixSquare;
            }
        }

        return matrix;
    }

    void InitializeAnswerObject() 
    {
        _answerMatrix = new GameObject();
        _answerMatrix.name = "AnswerMatrix";
        _answerMatrixSquaresData = GenerateMatrixSquares("AnswerSquare", true);
        for (int i = 0; i < _answerMatrixProportion; i++)
            for (int j = 0; j < _answerMatrixProportion; j++)
                _answerMatrixSquaresData[i, j].transform.parent = _answerMatrix.transform;
        _answerMatrix.tag = "Square2";

        BoxCollider2D bc = _answerMatrix.AddComponent<BoxCollider2D>();
        Transform transform = _answerMatrixSquaresData[_answerMatrixProportion / 2, _answerMatrixProportion / 2].transform;
        bc.offset = new Vector2(transform.position.x, transform.position.y);
        bc.size += new Vector2(_answerMatrix.transform.localScale.x / 10, _answerMatrix.transform.localScale.y / 10);
    }

    public void UpdateAnswerObject()
    {
        Vector3 lastPosition = _answerMatrix.transform.position;

        int[] validCoords = new int[_matrixProportion - _answerMatrixProportion + 1];

        for (int i = 0; i < validCoords.Length; i++)
            validCoords[i] = 0;

        _answerMatrix.transform.position = new Vector3(
            _matrixSquaresData[Random.Range(0, validCoords.Length), Random.Range(0, validCoords.Length)].transform.position.x 
                + _matrixSquaresData[_matrixSquaresData.GetLength(0) - 1, _matrixSquaresData.GetLength(1) - 1].transform.position.x
                - transform.position.x * 2,
            _matrixSquaresData[Random.Range(0, validCoords.Length), Random.Range(0, validCoords.Length)].transform.position.y 
                + _matrixSquaresData[_matrixSquaresData.GetLength(0) - 1, _matrixSquaresData.GetLength(1) - 1].transform.position.y
                - transform.position.y * 2);

        if (_answerMatrix.transform.position.Equals(lastPosition))
            UpdateAnswerObject();
    }

    public void UpdateMatrixSquareColors(Color[] color) => UpdateSquareColors(_matrixSquaresData, color, 1f);
    public void UpdateAnswerSquareColors(Color[] color, float alpha) => UpdateSquareColors(_answerMatrixSquaresData, color, alpha);

    void UpdateSquareColors(GameObject[,] squaresData, Color[] color, float alpha) 
    {
        foreach (GameObject square in squaresData)
        {
            Color squareColor =  color[Random.Range(0, color.Length)];
            squareColor.a = alpha;
            square.GetComponent<SpriteRenderer>().color = squareColor;
        }
    }
}
