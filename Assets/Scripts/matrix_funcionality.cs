using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class matrix_funcionality : MonoBehaviour
{
    public Camera mainKamera;
    private TurnManager _turnManager;
    private RaycastHit2D raycastHit;
    private int colliseManager = 0;
    public GameObject Acierto;
    public GameObject Error;
    private float _SFXVolume;

    // Start is called before the first frame update
    void Start()
    {
        mainKamera = Camera.main;
        _turnManager = GameObject.Find("Matrix").GetComponent<TurnManager>();
       
    }

    // Update is called once per frame
    void Update()
    {
        _SFXVolume = PlayerPrefs.GetFloat("SFX volume");
        if (Input.GetMouseButtonDown(0))
        {
            raycastHit = Physics2D.Raycast(mainKamera.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            if (raycastHit.collider == null)
            {
                colliseManager = 0;
                Debug.Log("0");
            }
            else if (raycastHit.collider.GameObject())
            {
                if (raycastHit.collider.tag == "Square2")
                {
                    Instantiate(Acierto).GetComponent<AudioSource>().volume = _SFXVolume;
                    colliseManager = 2;
                    Debug.Log("2");
                }
                else
                {
                    Instantiate(Error).GetComponent<AudioSource>().volume = _SFXVolume;
                    colliseManager = 1;
                    Debug.Log("1");
                }
            }

            _turnManager.CheckTurn(colliseManager);
        }
        //Guardem el collise Manager per utilitzarlo en el funcionament
        PlayerPrefs.GetInt("ColliseManager", colliseManager);
    }
}
