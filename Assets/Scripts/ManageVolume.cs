using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManageVolume : MonoBehaviour
{
    public Slider MusicVolume;
    public Slider SFXVolume;

    // Update is called once per frame
    public void Awake()
    {
        MusicVolume.value = 5f;
        SFXVolume.value = 5f;
    }
    void Update()
    {
        PlayerPrefs.SetFloat("Music volume", MusicVolume.value/10);
        PlayerPrefs.SetFloat("SFX volume", SFXVolume.value/10);
    }
}
