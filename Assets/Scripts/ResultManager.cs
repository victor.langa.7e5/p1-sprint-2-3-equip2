using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResultManager : MonoBehaviour
{
    float greyResult;
    float redResult;
    float greenResult;
    float blueResult;
    int maxSize = 300;
    int maxPoints = 1000;
    TMPro.TMP_Text ResultText;
    // Start is called before the first frame update
    void Start()
    {
        LoadResults();
        SetResultBars();
        SetMessage();
    }

    private void SetMessage()
    {
        string Message = "You have:";
        if (greyResult < 300)
            Message += " achromatopsia";
        if (redResult < 300)
            Message += " protanopia";
        if (greenResult < 300)
            Message += " deuteranopia";
        if (blueResult < 300)
            Message += " tritanopia";
        if (Message == "You have:")
            Message = "You see perfectly";
        ResultText.text = Message;
    }

    private void SetResultBars()
    {
        GameObject.Find("Bar1").GetComponent<RectTransform>()
            .sizeDelta = new Vector2(50, maxSize * (greyResult / maxPoints));
        GameObject.Find("Bar2").GetComponent<RectTransform>()
            .sizeDelta = new Vector2(50, maxSize * (redResult / maxPoints));
        GameObject.Find("Bar3").GetComponent<RectTransform>()
            .sizeDelta = new Vector2(50, maxSize * (greenResult / maxPoints));
        GameObject.Find("Bar4").GetComponent<RectTransform>()
            .sizeDelta = new Vector2(50, maxSize * (blueResult / maxPoints));
    }

    void LoadResults()
    {
        greyResult = PlayerPrefs.GetFloat("totalColorBlind");
        redResult = PlayerPrefs.GetFloat("redColorBlind");
        greenResult = PlayerPrefs.GetFloat("greenColorBlind");
        blueResult = PlayerPrefs.GetFloat("blueColorBlind");
        ResultText = GameObject.Find("ResultText").GetComponent<TMPro.TMP_Text>();
        GameObject.Find("Punctuation").GetComponent<TMPro.TMP_Text>().text = "Final score: " + PlayerPrefs.GetFloat("score") + " / 4000";
    }

}
