using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenMenu : MonoBehaviour
{
    public GameObject canvasOptions;
    public GameObject canvasUI;
    public GameObject mouseInput;
    bool gamePaused;


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OpenOptions();
        }
    }

    public void OpenOptions()
    {
        gamePaused = !gamePaused;

        if (gamePaused)
        {
            canvasUI.SetActive(false);
            canvasOptions.SetActive(true);
            mouseInput.SetActive(false);
            Time.timeScale = 0f;
        }
        else
        {
            mouseInput.SetActive(true);
            canvasUI.SetActive(true);
            canvasOptions.SetActive(false);
            Time.timeScale = 1f;
        }
    }
}